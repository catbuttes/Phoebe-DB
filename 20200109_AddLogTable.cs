using FluentMigrator;

namespace phoebe_db
{
    [Migration(202001091932)]
    public class AddLogTable : Migration
    {
        public override void Up()
        {
            Create.Schema("Logs");
            Create.Table("Log")
                .InSchema("Logs")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("DateTime").AsDateTime().WithDefault(SystemMethods.CurrentUTCDateTime)
                .WithColumn("Text").AsString();
        }

        public override void Down()
        {
            Delete.Table("Log").InSchema("Logs");
            Delete.Schema("Logs");
        }
    }
}