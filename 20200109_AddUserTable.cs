using FluentMigrator;

namespace phoebe_db
{
    [Migration(202001091943)]
    public class AddUserTable : Migration
    {
        public override void Up()
        {
            Create.Schema("StickyRoles");

            Create.Table("Guild")
                .InSchema("StickyRoles")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("DiscordId").AsInt64().NotNullable()
                .WithColumn("GuildName").AsString();

            Create.Table("Role")
                .InSchema("StickyRoles")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("DiscordId").AsInt64().NotNullable()
                .WithColumn("GuildId").AsInt64().NotNullable().ForeignKey("Guild", "StickyRoles", "Id")
                .WithColumn("RoleId").AsInt64().NotNullable()
                .WithColumn("RoleName").AsString().NotNullable();

            Create.Table("User")
                .InSchema("StickyRoles")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("DiscordId").AsInt64().NotNullable()
                .WithColumn("GuildId").AsInt64().NotNullable().ForeignKey("Guild", "StickyRoles", "Id")
                .WithColumn("UserName").AsString().NotNullable();

            Create.Table("UserRoles")
                .InSchema("StickyRoles")
                .WithColumn("Id").AsInt64().PrimaryKey().Identity()
                .WithColumn("RoleId").AsInt64().NotNullable().ForeignKey("Role", "StickyRoles", "Id")
                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("User", "StickyRoles", "Id");

        }

        public override void Down()
        {
            Delete.Table("UserRoles").InSchema("StickyRoles");
            Delete.Table("User").InSchema("StickyRoles");
            Delete.Table("Role").InSchema("StickyRoles");
            Delete.Table("Guild").InSchema("StickyRoles");
            Delete.Schema("StickyRoles");
        }
    }
}